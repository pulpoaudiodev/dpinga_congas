/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  SampleBin.hpp
//  SimpleSampler
//
//  Created by Rudolf Leitner on 08/11/16.
//
//

#ifndef SampleBin_hpp
#define SampleBin_hpp

#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include <queue>
#include <iostream>
#include <fstream>

#include "JuceHeader.h"

/*--------------------------------------------------------------------------------------------------------------
 * SampleBinSample
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class SampleBinSample {
public:
	std::string name;
	int sampleRate;
	int bitsPerFrame;
	int numberOfFrames;
	int startsAtByteInBin;
    int numberOfChannels;

	AudioSampleBuffer* framesBuffer;
    
    MemoryMappedAudioFormatReader* memoryMappedReader;


	bool appliesForDFD;

	SampleBinSample( 
            std::string aName,
			int aSampleRate,
			int aBitsPerFrame,
			int aNumberOfFrames,
			int aStartsAtByteInBin
	);
	~SampleBinSample();
    void resetAudioBuffers();
};



/*--------------------------------------------------------------------------------------------------------------
 * DFDRingBuffer
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class DFDRingBuffer{
public:
	bool available;
	AudioSampleBuffer buffer;
    float* inL;
    float* inR;
    bool buf0Requested;
    bool buf1Requested;
    bool buf0Ready;
    bool buf1Ready;
    
	DFDRingBuffer( int aNumChannels, int aNumberOfFrames );
    ~DFDRingBuffer();
	void reset();
    void resize( int aNumChannels, int aNumberOfFrames);
} ;




/*--------------------------------------------------------------------------------------------------------------
 * SampleBin
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class SampleBin {
public:
    std::string setupXML;
    juce::Image backgroundImg;
    
    std::map<std::string, juce::Image> images;

	std::string path;
	std::vector<SampleBinSample> binSamples;
	bool ready;

	ScopedPointer<FileInputStream> inStream;
	ScopedPointer<File> file;

    //DFD & RingBuffers
	std::vector<DFDRingBuffer*> dfdRingBuffer;
	bool isDfd;
    int dfdPreBufferSize;
    int dfdBufferSize;
    int dfdNrOfRingBuffers;
    int dfdLoadFramesAtSamplesLeft;
    
public:

	SampleBin();
    ~SampleBin();

    void resetAudioBuffers();

    //Sample access
    bool open( std::string aPath);
	bool isReady() { return ready;};
	void read();
	SampleBinSample* getSample( std::string aName);
	SampleBinSample* getSample( int ndx);
	int getNrOfBinSamples() { return binSamples.size();};
	void loadAudioData(SampleBinSample* sampleDef);
    void reloadAudioData();

    //DFD & RingBuffers
    void setupDfd( bool aIsDfd, int aNrOfRingBuffers, int  aDfdPreBufferSize, int aDfdBufferSize, int aDfdLoadFramesAtSamplesLeft);
    void changeDfd( bool aIsDfd, int aNrOfRingBuffers, int aDfdPreBufferSize, int aDfdBufferSize, int aDfdLoadFramesAtSamplesLeft);
    void createRingBuffers();
    void deleteRingBuffers();
    void resizeRingBuffers();
	DFDRingBuffer* requestRingBuffer();
	void returnRingBuffer( DFDRingBuffer* aRingBuffer );
    
    std::string& getSetupXML() { return setupXML;};
    bool loadBackgroundFromFile( std::string aFileName);
    bool loadSetupXMLFromFile( std::string aFileName);
    
    juce::Image& getBackgroundImage() { return backgroundImg;};
    juce::Image* getImage( std::string aName );
    
    
};







#endif /* SampleBin_hpp */



