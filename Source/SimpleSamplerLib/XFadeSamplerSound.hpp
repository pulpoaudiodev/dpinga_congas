/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  XFadeSamplerSound.hpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 03/12/16.
//
//

#ifndef XFadeSamplerSound_hpp
#define XFadeSamplerSound_hpp

#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include "JuceHeader.h"
#include "PatchDefinition.hpp"
#include "SampleBin.hpp"


/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerSoundElement
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/


struct XFadeSamplerSoundElement{
	std::vector<SampleBinSample*> roundRobinBinSamples;
	SoundElementDefinition::ROUNDROBINMODE roundRobinMode;
	int lastRRNdx;


	XFadeSamplerSoundElement() {
		lastRRNdx = -1;
	}	

	void addRoundrobinBinSample( SampleBinSample* aSampleBinSample) {
		roundRobinBinSamples.push_back( aSampleBinSample );
	}
	
	int getNrOfRoundRobinBinSamples() {
		return roundRobinBinSamples.size();
	};
	
	int getNrOfRoundRobinSounds() {
		if( roundRobinMode == SoundElementDefinition::ROUNDROBINMODE::OFF && roundRobinBinSamples.size() > 0) {
	  	return 1;
	  } else {
	  	return roundRobinBinSamples.size();
	  }
	}		
	
	SampleBinSample* getRoundRobinBinSample( int aRRNdx ) {
  	if( aRRNdx > -1 && aRRNdx < (int)roundRobinBinSamples.size()) {
  		lastRRNdx = aRRNdx;
  		return roundRobinBinSamples[aRRNdx];
  	} else {
  		return NULL;
  	}		
  }	
  
	SampleBinSample* getRoundRobinBinSample() {
		SampleBinSample* sbs = NULL;
		int nRR = getNrOfRoundRobinSounds();
		int newRRNdx=0;
		switch( roundRobinMode ) {
			case SoundElementDefinition::ROUNDROBINMODE::OFF:
				if( roundRobinBinSamples.size() > 0) {
					sbs =  roundRobinBinSamples[0];
				}	
				break;
			case SoundElementDefinition::ROUNDROBINMODE::CYCLE:
				if( nRR > 0) {
					newRRNdx = lastRRNdx +1;
					if( newRRNdx == nRR) {
						newRRNdx = 0;
					}
				} 
				sbs =  roundRobinBinSamples[newRRNdx];
				lastRRNdx = newRRNdx;
				break;
			case SoundElementDefinition::ROUNDROBINMODE::RANDOM:
				newRRNdx = rand() % nRR;
				sbs =  roundRobinBinSamples[newRRNdx];
				lastRRNdx = newRRNdx;
				break;
		}		
		return sbs;				
	}
} ;




/*--------------------------------------------------------------------------------------------------------------
 * XFadeSamplerSound
 *
 *
 *
 ---------------------------------------------------------------------------------------------------------------*/

class   XFadeSamplerSound    : public SynthesiserSound
{
public:
	//==============================================================================

	std::vector<XFadeSamplerSoundElement> soundElements;
	SoundDefinition* soundDefinition;

	bool appliesTo( int aSoundSet, int midiNoteNumber);


	XFadeSamplerSound(
			SoundDefinition* aSoundDefinition,
            SampleBin* aSampleBin,
            std::string& errors
	);

	/** Destructor. */
	virtual ~XFadeSamplerSound();


	bool appliesToNote (int midiNoteNumber) override;
	bool appliesToChannel (int midiChannel) override;
    
    bool isReady() {return ready;};
    
private:
    bool ready;

public:

	JUCE_LEAK_DETECTOR (XFadeSamplerSound)
};



#endif /* XFadeSamplerSound_hpp */
