/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  UIControls.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 11/12/16.
//
//

#include "UIControls.hpp"
#include <sstream>
#include "../ProductDef.h"
//#include "UIImages.h"



/*====================================================================================================
        KnobManImage
====================================================================================================*/


KnobManImage::KnobManImage ( Image *aKnobManImg, int aWidth, int aHeight, int aCount) : knobManImg(aKnobManImg)
{
    width = aWidth;
    height = aHeight;
    count = aCount;

}


void KnobManImage::drawFrame (Graphics &g, int x, int y, int aWidth, int /*aHeight*/,  float imgNdx)
{
	double pos			= (int)(imgNdx * count);
    if (pos > 0)
    	pos = pos - 1;

    x = (aWidth-width) / 2;
g.drawImage (*knobManImg, x,y, width, height, 0, (int)(pos*height), width, height, false);
}










/*====================================================================================================
 MyLookAndFeel
 ====================================================================================================*/



MyLookAndFeel::MyLookAndFeel()
{
	rotarySliderKnobManImg = nullptr;
    toggleButtonKnobManImg = nullptr;
    
    /*setColour (Slider::textBoxHighlightColourId, Colour(TEXTBOXHIGHLIGHTCOLOUR));
    setColour( Slider::textBoxTextColourId, Colour(TEXTBOXTTEXTCOLOUR));
    setColour( Slider::textBoxBackgroundColourId, Colour (TEXTBOXBACKGROUNDCOLOUR));
    setColour( Slider::textBoxOutlineColourId, Colour (TEXTBOXOUTLINECOLOUR));
    setColour( ComboBox::backgroundColourId, Colour(COMBOBOXMACKGROUNDCOLOUR));
    setColour( PopupMenu::backgroundColourId,Colour(POPUPMENUBACKGROUNDCOLOUR));
    setColour( TextButton::buttonColourId,Colour(TEXTBUTTONCOLOUR));*/
    
}


MyLookAndFeel::~MyLookAndFeel()
{
	if( rotarySliderKnobManImg != NULL ) {
		delete rotarySliderKnobManImg;
	}
    if( toggleButtonKnobManImg != NULL ) {
        delete toggleButtonKnobManImg;
    }
}

void MyLookAndFeel::setColours(
                               Colour& aTextBoxBackgroundColour,
                               Colour& aTextBoxTextColour,
                               Colour& aTextBoxTextEditColour,
                               Colour& aTextBoxHighlightColour,
                               Colour& aTextBoxHighlightedTextColour,
                               Colour& aTextBoxOutLineColour,
                               Colour& aTextBoxFocusedOutLineColour,
                               Colour& aTextBoxShadowColour,
                               Colour& aComboBoxBackgroundColour,
                               Colour& aPopupMenuBackgroundColour,
                               Colour& aTextButtonColour
                               )
                               
{
    setColour( Slider::textBoxBackgroundColourId, aTextBoxBackgroundColour);
    setColour( Slider::textBoxTextColourId, aTextBoxTextColour);
    setColour( Slider::textBoxHighlightColourId, aTextBoxHighlightColour);
    setColour( TextEditor::highlightedTextColourId, aTextBoxHighlightedTextColour);
    setColour( Slider::textBoxOutlineColourId, aTextBoxOutLineColour);
    setColour( TextEditor::focusedOutlineColourId, aTextBoxFocusedOutLineColour);
    setColour( TextEditor::shadowColourId, aTextBoxShadowColour);
    setColour( TextEditor::textColourId, aTextBoxTextEditColour);
    setColour( TextEditor::backgroundColourId, Colours::black);
    
    setColour( ComboBox::backgroundColourId, aComboBoxBackgroundColour);
    setColour( PopupMenu::backgroundColourId,aPopupMenuBackgroundColour);
    setColour( TextButton::buttonColourId,aTextButtonColour);
}

void MyLookAndFeel::setRotarySliderKnobManImage( Image *aRotarySliderKnobManImg, int _frameWidth, int _frameHeight, int _frameCount )
{
	if( rotarySliderKnobManImg != NULL) {
		delete rotarySliderKnobManImg;
	}
	rotarySliderKnobManImg = new KnobManImage(aRotarySliderKnobManImg, _frameWidth, _frameHeight, _frameCount );
}

void MyLookAndFeel::setToggleButtonKnobManImage( Image *aToggleSliderKnobManImg, int _frameWidth, int _frameHeight, int _frameCount )
{
    if( toggleButtonKnobManImg != NULL) {
        delete toggleButtonKnobManImg;
    }
    toggleButtonKnobManImg = new KnobManImage(aToggleSliderKnobManImg, _frameWidth, _frameHeight, _frameCount  );
    
}


void MyLookAndFeel::drawRotarySlider (Graphics& g, int x, int y, int width, int height, float sliderPos,
                                       const float rotaryStartAngle, const float rotaryEndAngle, Slider& slider)
{
    if( rotarySliderKnobManImg != nullptr ) {
        rotarySliderKnobManImg->drawFrame (g, x, y, width, height,  sliderPos);
    } else {
       LookAndFeel_V3::drawRotarySlider(g,x,y,width,height,sliderPos,rotaryStartAngle,rotaryEndAngle,slider);
    }

}


void MyLookAndFeel::drawToggleButton 	(Graphics& g,ToggleButton& tb ,bool isMouseOverButton, bool isButtonDown )
{
    if( toggleButtonKnobManImg != nullptr ) {
        int xCenter = (tb.getWidth() / 2) - toggleButtonKnobManImg->width/2;
        toggleButtonKnobManImg->drawFrame (g, xCenter, 0, tb.getWidth(), tb.getHeight(), (float) (tb.getToggleState()? 1:0));
    } else {
        LookAndFeel_V3::drawToggleButton(g,tb,isMouseOverButton,isButtonDown);
    }
}



/*--------------------------------------------------------------------------------------------------------------
  KeyRangeMidiKeyboardComponent
 ---------------------------------------------------------------------------------------------------------------*/
KeyRangeMidiKeyboardComponent::KeyRangeMidiKeyboardComponent (MidiKeyboardState& state,
                       	   	   	   	   	   	   	   	   	   Orientation orientation)
:MidiKeyboardComponent(state, orientation)
{
	playRangeFromNote = -1;
	playRangeToNote = -1;
	keyswitchRangeFromNote = -1;
	keyswitchRangeToNote = -1;
}


void KeyRangeMidiKeyboardComponent::setPlayRange( int aRangeFromNote, int aRangeToNote)
{
	playRangeFromNote = aRangeFromNote;
	playRangeToNote = aRangeToNote;
	repaint();
}

void  KeyRangeMidiKeyboardComponent::setKeyswitchRange( int aRangeFromNote, int aRangeToNote)
{
	keyswitchRangeFromNote = aRangeFromNote;
	keyswitchRangeToNote = aRangeToNote;
	repaint();
}


void KeyRangeMidiKeyboardComponent::setPlayRangeShift( int aShift )
{
	playRangeShift = aShift;
	repaint();
}

void KeyRangeMidiKeyboardComponent::setKeyswitchRangeShift( int aShift )
{
	keyswitchRangeShift = aShift;
	repaint();
}

void KeyRangeMidiKeyboardComponent::centerVisibleRange()
{

    int toDispLowest = (playRangeFromNote+playRangeShift);
    if( keyswitchRangeFromNote > -1 && (keyswitchRangeFromNote+keyswitchRangeShift)<toDispLowest)
        toDispLowest =keyswitchRangeFromNote+keyswitchRangeShift;
    
    int toDispHighest = (playRangeToNote+playRangeShift);
    if( keyswitchRangeToNote > -1 && (keyswitchRangeToNote+keyswitchRangeShift)>toDispHighest)
        toDispHighest =keyswitchRangeToNote+keyswitchRangeShift;
    
    if( getLowestVisibleKey() > toDispLowest)
        setLowestVisibleKey(toDispLowest);    
}



bool KeyRangeMidiKeyboardComponent::isKeyswitchNote( int midiNoteNumber )
{
	return( midiNoteNumber >= (keyswitchRangeFromNote+keyswitchRangeShift) &&  midiNoteNumber <= (keyswitchRangeToNote+keyswitchRangeShift));
}

bool KeyRangeMidiKeyboardComponent::isPlayNote( int midiNoteNumber)
{
	return( midiNoteNumber >= (playRangeFromNote+playRangeShift) &&  midiNoteNumber <= (playRangeToNote+playRangeShift));
}


void  KeyRangeMidiKeyboardComponent::drawWhiteNote (int midiNoteNumber,
                                           Graphics& g, int x, int y, int w, int h,
                                           bool isDown, bool isOver,
                                           const Colour& lineColour,
                                           const Colour& textColour) 
{
    Colour c (Colours::transparentWhite);
    if (isDown)  c = c.overlaidWith( findColour (keyDownOverlayColourId));
    if (isOver)  c = c.overlaidWith (findColour (mouseOverKeyOverlayColourId));

    g.setColour (c);
    g.fillRect (x, y, w, h);


    const String text (getWhiteNoteText (midiNoteNumber));

    if (text.isNotEmpty())
    {
        const float fontHeight = jmin (12.0f, getKeyWidth() * 0.9f);

        g.setColour (textColour);
        g.setFont (Font (fontHeight).withHorizontalScale (0.8f));

        switch (getOrientation())
        {
            case horizontalKeyboard:            g.drawText (text, x + 1, y,     w - 1, h - 2, Justification::centredBottom, false); break;
            case verticalKeyboardFacingLeft:    g.drawText (text, x + 2, y + 2, w - 4, h - 4, Justification::centredLeft,   false); break;
            case verticalKeyboardFacingRight:   g.drawText (text, x + 2, y + 2, w - 4, h - 4, Justification::centredRight,  false); break;
            default: break;
        }
    }

    if (! lineColour.isTransparent())
    {
        g.setColour (lineColour);

        switch (getOrientation())
        {
            case horizontalKeyboard:            g.fillRect (x, y, 1, h); break;
            case verticalKeyboardFacingLeft:    g.fillRect (x, y, w, 1); break;
            case verticalKeyboardFacingRight:   g.fillRect (x, y + h - 1, w, 1); break;
            default: break;
        }

        if (midiNoteNumber == getRangeEnd())
        {
            switch (getOrientation())
            {
                case horizontalKeyboard:            g.fillRect (x + w, y, 1, h); break;
                case verticalKeyboardFacingLeft:    g.fillRect (x, y + h, w, 1); break;
                case verticalKeyboardFacingRight:   g.fillRect (x, y - 1, w, 1); break;
                default: break;
            }
        }
    }
    if (isKeyswitchNote(midiNoteNumber)) {
        Colour cKs(Colours::blue);
        if (isDown)  cKs = cKs.overlaidWith( findColour (keyDownOverlayColourId));
        if (isOver)  cKs = cKs.overlaidWith (findColour (mouseOverKeyOverlayColourId));
        g.setColour(cKs);
        g.fillRect(x,y,w,5);
    }
    if (isPlayNote(midiNoteNumber)) {
        Colour cKs(Colours::green);
        if (isDown)  cKs = cKs.overlaidWith( findColour (keyDownOverlayColourId));
        if (isOver)  cKs = cKs.overlaidWith (findColour (mouseOverKeyOverlayColourId));
        g.setColour(cKs);
        g.fillRect(x,y,w,5);
    }
    
}

void  KeyRangeMidiKeyboardComponent::drawBlackNote (int midiNoteNumber,
                                           Graphics& g, int x, int y, int w, int h,
                                           bool isDown, bool isOver,
                                           const Colour& noteFillColour)
{
    Colour c (noteFillColour);

    if (isDown)  c = c.overlaidWith (findColour (keyDownOverlayColourId));
    if (isOver)  c = c.overlaidWith (findColour (mouseOverKeyOverlayColourId));

    g.setColour (c);
    g.fillRect (x, y, w, h);
    
    

    if (isDown)
    {
        g.setColour (noteFillColour);
        g.drawRect (x, y, w, h);
    }
    else
    {
        g.setColour (c.brighter());
        const int xIndent = jmax (1, jmin (w, h) / 8);

        switch (getOrientation())
        {
            case horizontalKeyboard:            g.fillRect (x + xIndent, y, w - xIndent * 2, 7 * h / 8); break;
            case verticalKeyboardFacingLeft:    g.fillRect (x + w / 8, y + xIndent, w - w / 8, h - xIndent * 2); break;
            case verticalKeyboardFacingRight:   g.fillRect (x, y + xIndent, 7 * w / 8, h - xIndent * 2); break;
            default: break;
        }
    }
    if (isKeyswitchNote(midiNoteNumber)) {
        Colour cKs(Colours::blue);
        if (isDown)  cKs = cKs.overlaidWith( findColour (keyDownOverlayColourId));
        if (isOver)  cKs = cKs.overlaidWith (findColour (mouseOverKeyOverlayColourId));
        g.setColour(cKs);
        g.fillRect(x,y,w,5);
    }
    if (isPlayNote(midiNoteNumber)) {
        Colour cKs(Colours::green);
        if (isDown)  cKs = cKs.overlaidWith( findColour (keyDownOverlayColourId));
        if (isOver)  cKs = cKs.overlaidWith (findColour (mouseOverKeyOverlayColourId));
        g.setColour(cKs);
        g.fillRect(x,y,w,5);
    }
    
    
}
