/******************************************************************
 This software has been developed 2017 by Rudi Leitner
 as open source under the GPL3 license.
 
 It uses the open source version of JUCE GRAPEFRUIT under GPL3.
 *******************************************************************/
//
//  PluginParameters.cpp
//  SimpleSamplerComponent
//
//  Created by Rudolf Leitner on 27/11/16.
//
//

#include "PluginParameters.hpp"


/*--------------------------------------------------------------------------------------------------------------
 * PluginParameters
 *
 * ADAPTER for all Plugin Parameters
 * Although we can somehow access theses parameters by  processor::getParameter( ndx),
 * we dont want this because some parameters like gain are used for calculation of each seperate sample and
 * may respond immediately.  So this collection of pointers will use less CPU.
 ---------------------------------------------------------------------------------------------------------------*/

PluginParameters::PluginParameters()
{
    veloSensParam = NULL;
    gainParam = NULL;
    tuneStParam = NULL;
    tuneParam = NULL;
    panParam = NULL;
    bevelParam = NULL;

    keySwitchRangeShiftParam = NULL;
    playRangeShiftParam = NULL;

    offsetTParam = NULL;
    attackTParam = NULL;
    holdTParam = NULL;
    decayTParam = NULL;
    sustainGainParam = NULL;
    releaseTParam = NULL;
    
    for( int i=0; i<MAX_NR_OF_LAYERS; i++) {
        layerGainParams[i] = NULL;
    }
    
    pressureModeParam = NULL;
    pressureControllerParam = NULL;
    
    pitchWheelModeParam = NULL;
    pitchWheelRangeParam = NULL;
    pitchWheelRetriggerReleaseTParam = NULL;
    pitchWheelRetriggerOffsetTParam = NULL;
    pitchWheelRetriggerAttackTParam = NULL;
    
    legatoModeParam = NULL;
    legatoReleaseTParam = NULL;
    legatoOffsetTParam = NULL;
    legatoAttackTParam = NULL;

    monoModeParam = NULL;

    soundSetParam = NULL;

    //dfdParam = false;
    //dfdLoadFrames = 0;
    //dfdPreloadFrames = -1;

    dfdParam = NULL;
    dfdPreloadFrames = NULL;
    dfdLoadFrames = NULL;
    dfdLoadAtFramesLeft = NULL;


}


